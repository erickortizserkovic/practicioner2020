function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Erick",
    apellidos:"Ortiz Serkovic",
    ciudad:"Lima",
    pais:"Peru"
  };
  sessionStorage.setItem("json", JSON.stringify(objeto));
}
function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}
function borrarDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.removeItem(clave, valor);
}
function clearAllDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.clear();
}
